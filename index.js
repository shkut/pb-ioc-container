let CreateContainer = function (lazy = true) {
	let dependencies = {},
		factories = {},
		container = {},
		chain = [],
		chainForGraph = [],
		graph = {},
		entryPoints = [];

	//Return true if such factory already exists
	container.factory = (name, factory) => {
		let exists = !!factories[name];
		factories[name] = factory;
		if (lazy === false) {
			dependencies[name] = factory(proxy);
		}
		return exists;
	};

	//Return true if such dependency already exists
	container.register = (name, instance) => {
		let exists = !!dependencies[name];
		dependencies[name] = instance;
		return exists;
	};
	container.set = container.register;

	container.factoriesFromObject = (obj) => {
		Object.keys(obj).forEach((key) => {
			container.factory(key, obj[key]);
		});
	};

	let proxy = new Proxy({}, {
		get(target, property) {
			return container.get(property);
		}
	});

	const get = (name) => {
		if (!dependencies[name]) {
			const factory = factories[name];
			if (!factory) {
				throw new Error(
					`Neither dependency nor factory [${name}] is not declared. Make sure it returns a function or an object.`
				);
			}
			dependencies[name] = factory(proxy);
			if (!dependencies[name]) {
				throw new Error(
					`Instantiated factory [${name}] is not valid. Make sure it returns a function or an object.`
				);
			}
		}
		return dependencies[name];
	};

	const checkForRecursion = (chain, name) => {
		chain.forEach((elem) => {
			if (elem === name) {
				let way = chain.reduce((prev, cur) => {
					return `(${prev}) => (${cur})`;
				});
				way += ` => (${name})`;
				throw new Error(
					'Dependency loop detected. Please, split your dependencies into more isolated modules.\n' +
					'Chain of currently nesting dependencies:\n' +
					`[ ${way} ]` +
					'\n'
				);
			}
		});
	};

	container.get = (name) => {
		let current = chainForGraph[chainForGraph.length - 1];
		checkForRecursion(chainForGraph, name);
		if (current) {
			graph[current] = graph[current] || [];
			graph[current].push(name);
		} else {
			entryPoints.push(name);
		}
		chain.push(name);
		chainForGraph.push(name);
		let result = null;
		try {
			result = get(name);
		} catch (err) {
			let way = chainForGraph.reduce((prev, cur) => prev ? `${prev} => (${cur})` : `(${cur})`, null);
			console.error(
				`Error occured trying to get dependency [${name}]. Current execution chain:` +
				'\nChain of currently nesting dependencies:\n' +
				`[ ${way} ]` +
				'\n'
			);
			throw (err);
		}
		chainForGraph.pop();
		return result;
	};

	container.hasDependency = (name) => !!dependencies[name];
	container.hasFactory = (name) => !!factories[name];
	container.has = (name) => !!factories[name] || !!dependencies[name];
	container.getUninstantiatedFactories = () => Object.keys(factories).filter((value) => !dependencies[value]);
	container.getInstantiatedFactories = () => Object.keys(factories).filter((value) => !!dependencies[value]);
	container.getChain = () => {
		return chain;
	};
	container.getGraph = () => {
		return graph;
	};
	container.getEntryPoints = () => {
		return entryPoints;
	};

	container.set('_srvlc', container);

	return container;
};

module.exports = CreateContainer;